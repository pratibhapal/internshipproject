from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import generics
from .models import (Categories, SubCategories, levelCategories, QuestionOptionAns, ExamDone)
from .serializer import (CategoriesSerializer,SubCategoriesSerializer,levelCategoriesSerializer, QuestionOptionAnsSerializer,ExamDoneSerializer)
from django.template import loader
# Create your views here.
# 1
class ListCreateCategoriesView(generics.ListCreateAPIView):
	queryset = Categories.objects.all()
	serializer_class = CategoriesSerializer

class CategoriesDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = Categories.objects.all()
	serializer_class = CategoriesSerializer

# 2
class ListCreateSubCategoriesView(generics.ListCreateAPIView):
	queryset = SubCategories.objects.all()
	serializer_class = SubCategoriesSerializer

class SubCategoriesDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = SubCategories.objects.all()
	serializer_class = SubCategoriesSerializer
# 3
class ListCreatelevelCategoriesView(generics.ListCreateAPIView):
	queryset = levelCategories.objects.all()
	serializer_class = levelCategoriesSerializer

class levelCategoriesDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = levelCategories.objects.all()
	serializer_class = levelCategoriesSerializer
# 4
class ListCreateQuestionOptionAnsView(generics.ListCreateAPIView):
	queryset = QuestionOptionAns.objects.all()
	serializer_class = QuestionOptionAnsSerializer

class QuestionOptionAnsDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = QuestionOptionAns.objects.all()
	serializer_class = QuestionOptionAnsSerializer
# 5
class ListCreateExamDoneView(generics.ListCreateAPIView):
	queryset = ExamDone.objects.all()
	serializer_class = ExamDoneSerializer

class ExamDoneDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = ExamDone.objects.all()
	tmplateLoader = loader.get_template('index.html')
	context = {
		'questionOption':queryset,
	}
	HttpResponse(template.render(context,request))
	# serializer_class = ExamDoneSerializer