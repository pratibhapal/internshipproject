from django.apps import AppConfig


class ApiservicesConfig(AppConfig):
    name = 'ApiServices'
