from rest_framework import serializers
from .models import (Categories, SubCategories, levelCategories, QuestionOptionAns, ExamDone)

class CategoriesSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = Categories

class SubCategoriesSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = SubCategories

class levelCategoriesSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = levelCategories

class QuestionOptionAnsSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = QuestionOptionAns

class ExamDoneSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = ExamDone
