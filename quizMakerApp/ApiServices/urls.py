from django.urls import path
from .views import ListCreateCategoriesView, CategoriesDetailView, ListCreateSubCategoriesView, SubCategoriesDetailView,ListCreatelevelCategoriesView, levelCategoriesDetailView,ListCreateQuestionOptionAnsView,QuestionOptionAnsDetailView,ListCreateExamDoneView ,ExamDoneDetailView
urlpatterns = [
path('categories/',ListCreateCategoriesView.as_view(),name="list-create-categories"),
path('categories/<uuid:pk>',CategoriesDetailView.as_view(),name="update-delete-categories"),
path('sub-categories/',ListCreateSubCategoriesView.as_view(),name="list-create-subcategories"),
path('sub-categories/<uuid:pk>',SubCategoriesDetailView.as_view(),name="update-delete-subcategories"),
path('questionlevel/',ListCreatelevelCategoriesView.as_view(),name="list-create-questionlevel"),
path('questionlevel/<uuid:pk>',levelCategoriesDetailView.as_view(),name="update-delete-questionlevel"),
path('questions/',ListCreateQuestionOptionAnsView.as_view(),name="list-create-questions"),
path('questions/<uuid:pk>',QuestionOptionAnsDetailView.as_view(),name="update-delete-questions"),
path('exams/',ListCreateExamDoneView.as_view(),name="list-create-examspaper"),
path('exams/<uuid:pk>',ExamDoneDetailView.as_view(),name="update-delete-examspaper"),
]