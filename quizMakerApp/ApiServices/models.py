from django.db import models
from datetime import datetime
import uuid
# Create your models here.

class Categories(models.Model):
	cat_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	cat_name = models.CharField(max_length=200)
	cat_status = models.IntegerField(default=1)
	def __str__(self):
		return self.cat_name

class SubCategories(models.Model):
	subcat_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	cat_id  = models.ForeignKey(Categories,on_delete=models.CASCADE)
	subcat_name = models.CharField(max_length=200)
	subcat_status = models.IntegerField(default=1)
	def __str__(self):
		return self.subcat_name

class levelCategories(models.Model):
	lvl_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	lvl_name = models.CharField(max_length=200)
	def __str__(self):
		return self.lvl_name

class QuestionOptionAns(models.Model):
	Question_id 	= models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	Questions 		= models.CharField(max_length=2000)
	Question_option1 = models.CharField(max_length=2000)
	Question_option2 = models.CharField(max_length=2000)
	Question_option3 = models.CharField(max_length=2000)
	Question_option4 = models.CharField(max_length=2000)
	Question_ans 	= models.CharField(max_length=2000)
	Question_cat 	= models.ForeignKey(Categories,on_delete=models.CASCADE)
	Question_subcat = models.ForeignKey(SubCategories,on_delete=models.CASCADE)
	Question_lvl 	= models.ForeignKey(levelCategories,on_delete=models.CASCADE)
	def __str__(self):
		return self.Question_id

class ExamDone(models.Model):
	exam_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	exam_name = models.CharField(max_length=200)
	exam_qid = models.ForeignKey(QuestionOptionAns,on_delete=models.CASCADE)
	exam_ans =  models.CharField(max_length=2000)
	exam_time = models.CharField(max_length=2000)
	inseted_ts = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return str(self.exam_id)

